#!/bin/bash

echo "setting ROOT & yaml..."
source /cvmfs/sft.cern.ch/lcg/views/LCG_97rc4python3/x86_64-centos7-gcc9-opt/setup.sh

export MMCR_DS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
export MMCR_DS_BUILD_DIR="${MMCR_DS_DIR}/../build"
export MMCR_DS_INSTALL_DIR="${MMCR_DS_DIR}/../install"
echo "setting project in ${MMCR_DS_DIR}/.. ..."
mkdir -vp ${MMCR_DS_BUILD_DIR} 
mkdir -vp ${MMCR_DS_INSTALL_DIR}

export PATH="${MMCR_DS_INSTALL_DIR}/:$PATH"
export LD_LIBRARY_PATH="${MMCR_DS_INSTALL_DIR}/lib:$LD_LIBRARY_PATH"
