#include <iostream>

#include "controller.h"

int main()
{
    std::cout << "Hello world" << std::endl;

    Controller &mController = Controller::GetInstance();
    mController.Print();
}
