#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <iostream>

class Controller
{
public:
    static Controller& GetInstance()
    {
        static Controller controller;
        return controller;
    }

    void Print()
    {
        std::cout << "In controller:" << std::endl;
    }

private:
    Controller() {}
    ~Controller() {};
};

#endif
